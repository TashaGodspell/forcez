﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickUp : MonoBehaviour {

	public Animator anim;
	// Use this for initialization
	void Start () {
		anim = GameObject.FindGameObjectWithTag ("Player").GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.transform.tag == "Player") {
			anim.SetBool ("WeaponOne", true);
			gameObject.GetComponent<SpriteRenderer> ().enabled = false;
		}
	}

}
