﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {

	public EventSystem es;
	public GameObject selectedGameObject;
	public bool buttonSelected;

	void Start () {
		
	}
	
	void Update () {
		if (Input.GetAxisRaw ("Vertical") != 0 && buttonSelected == false){
			es.SetSelectedGameObject(selectedGameObject);
			buttonSelected = true;
		}
	}

	private void OnDisable() {
		buttonSelected = false;
	}
}

