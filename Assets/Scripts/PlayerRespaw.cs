﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRespaw : MonoBehaviour {


	private PLControle thePLayer;
	private CamControle theCamera;

	public Vector2 startDirection;

	// Use this for initialization
	void Start () {
		thePLayer = FindObjectOfType<PLControle>();
		//muda a posição do player para a posição do empty object
		thePLayer.transform.position = transform.position;
		thePLayer.lastMove = startDirection;

		theCamera = FindObjectOfType<CamControle>();
		theCamera.transform.position = new Vector3 (transform.position.x, transform.position.y, theCamera.transform.position.z);


		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
