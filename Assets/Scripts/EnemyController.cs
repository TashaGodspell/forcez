﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	public float moveSpeed;

	private Rigidbody2D myRigidbody;
	private Animator anime;

	private bool EnemyMoving;
	public float  timeBetweenMove;
	private float TimeBetweenMoveCounter;

	public float timeToMove;
	private float TimeToMoveCounter;

	private float rand;
	private Vector3 moveDirection;


	// Use this for initialization
	void Start () {
		anime = GetComponent<Animator> ();

		myRigidbody = GetComponent<Rigidbody2D> ();

		//TimeBetweenMoveCounter = timeBetweenMove;
		//TimeToMoveCounter = timeToMove;

		TimeBetweenMoveCounter = Random.Range (timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
		TimeToMoveCounter = Random.Range (timeToMove * 0.75f, timeToMove * 1.25f);


	}
	
	// Update is called once per frame
	void Update () {
		if (EnemyMoving) {
			TimeToMoveCounter -= Time.deltaTime;
			myRigidbody.velocity = moveDirection;




			if (TimeToMoveCounter < 0f) {
				EnemyMoving = false;
				//TimeBetweenMoveCounter = timeBetweenMove;
				TimeBetweenMoveCounter = Random.Range (timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);

				anime.SetBool ("EnemyMoving", EnemyMoving);
			}

		} else {
			TimeBetweenMoveCounter -= Time.deltaTime;
			myRigidbody.velocity = Vector2.zero;

			if (TimeBetweenMoveCounter < 0f) {
				EnemyMoving = true;
				//TimeToMoveCounter = timeToMove;
				TimeToMoveCounter = Random.Range (timeToMove * 0.75f, timeBetweenMove * 1.25f);
				rand = Random.Range (-1f, 1f) * moveSpeed;
				//moveDirection = new Vector3 (Random.Range (-1f, 1f) * moveSpeed, Random.Range (-1f, 1f) * moveSpeed, 0f);
				moveDirection = new Vector3 (rand, rand, 0f);


				anime.SetBool ("EnemyMoving", EnemyMoving);
				anime.SetFloat ("MoveEX", rand);
				anime.SetFloat ("MoveEY", rand);
				anime.SetFloat ("LastMoveEX", rand);
				anime.SetFloat ("LastMoveEY", rand);

			}
		}



	}
}
