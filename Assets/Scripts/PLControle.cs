﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PLControle : MonoBehaviour {

	public float moveSpeed;

	private Animator anim;
	private Rigidbody2D myRigidbody;

	private static bool playerExists;
	private bool playermoving;
	private bool firstWeapon = true;

	public Vector2 lastMove;

	void Start () {
		//pega o counteudo do animator no começo pra saber quem ele é
		anim = GetComponent<Animator> ();
		myRigidbody = GetComponent<Rigidbody2D> ();

		if (!playerExists) {
			playerExists = true;
			//n destroi o gameobject q este script está ligado
			DontDestroyOnLoad (transform.gameObject);
		} else {

			Destroy (gameObject);
		}



	}

	void Update () {
		this.PlayerMovement();
	}

	void PlayerMovement () {
		playermoving = false;

		//direita e esquerda
		if (Input.GetAxisRaw ("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5 )
		{
			//move o personagem mas é ruim com colisão porq fica fazendo um efeito de salto
			//transform.Translate (new Vector3 (Input.GetAxisRaw ("Horizontal") * moveSpeed * Time.deltaTime, 0f, 0f));
			//melhor usar o rigidbody para mover
			myRigidbody.velocity = new Vector2(Input.GetAxisRaw ("Horizontal") * moveSpeed ,myRigidbody.velocity.y);
			playermoving = true;
			lastMove = new Vector2 (Input.GetAxisRaw ("Horizontal"), 0f);
		}
		//cima e baixo
		if (Input.GetAxisRaw ("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5 )
		{
			
			//transform.Translate (new Vector3 (0f , Input.GetAxisRaw ("Vertical") * moveSpeed * Time.deltaTime, 0f));
			myRigidbody.velocity = new Vector2(myRigidbody.velocity.x ,Input.GetAxisRaw ("Vertical")* moveSpeed);

			playermoving = true;
			lastMove = new Vector2 (0f, Input.GetAxisRaw ("Vertical"));
		}


		//como esta se movendo pelo rigidbody tem o efeito da inercia então precisa de um codigo para parar com as forças e o personagem n sair deslizando
		if (Input.GetAxisRaw ("Horizontal") < 0.5f && Input.GetAxisRaw ("Horizontal") > -0.5) {

			myRigidbody.velocity = new Vector2 (0f, myRigidbody.velocity.y);
		}


		if (Input.GetAxisRaw ("Vertical") < 0.5f && Input.GetAxisRaw ("Vertical") > -0.5) {

			myRigidbody.velocity = new Vector2 (myRigidbody.velocity.x, 0f);
		}

		//manda os valores pro animator
		anim.SetFloat ("MoveX", Input.GetAxisRaw ("Horizontal"));
		anim.SetFloat ("MoveY", Input.GetAxisRaw ("Vertical"));
		anim.SetBool  ("PlayerMoving", playermoving);
		anim.SetFloat ("LastMoveX", lastMove.x);
		anim.SetFloat ("LastMoveY", lastMove.y);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.transform.tag == "PickUp") {
			Debug.Log("colidiu");
		}
	}
}
