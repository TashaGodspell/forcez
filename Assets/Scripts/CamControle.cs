﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CamControle : MonoBehaviour {

	public GameObject followTarget;
	private Vector3 targetPos;
	public float moveSpeed;

	private static bool cameraExists;

	// Use this for initialization
	void Start () {

		if (!cameraExists) {
			cameraExists = true;
			//n destroi o gameobject q este script está ligado
			DontDestroyOnLoad (transform.gameObject);
		} else {

			Destroy (gameObject);
		}


	}
	
	// Update is called once per frame
	void Update () {
		//posição
		targetPos = new Vector3 (followTarget.transform.position.x, followTarget.transform.position.y, transform.position.z);
		//move a camera
		transform.position = Vector3.Lerp (transform.position, targetPos, moveSpeed * Time.deltaTime);

	}
}
